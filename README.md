#### Overview

The Jupyter notebook here is meant to serve as an example of basic beamforming in seismology. The "data" used is simulated in SALVUS, using a single, vertical ricker wavelet. 

The example notebook shows several different ways an algorithm might be formulated, including:

- in time domain - timeshifting one waveform to match another and measuring a correlation score (Bartlett processor)

- in time domain - timeshifting all waveforms to a common reference/origin point and measuring correlation score (same result as above, but faster)

- in time domain - timeshifting all waveforms to a common point and directly stacking / adding energy ("delay-and-sum")

- in frequency domain

- in frequency domain, with MUSIC as an image-enhancing example.

- in time domain, but by first computing a database of noise correlation functions, and then using beamforming to look up various lag times.




#### Installation

No explicit installation is required, assuming you can run a Jupyter Python Notebook.

To get started with Jupyter notebooks, I recommend using the conda or miniconda package manager: https://docs.conda.io/en/latest/miniconda.html

I also generally recommend using different environments, separate from the base or "root" environment. To install a new environment:

```
$ conda create -n beamforming_environment python=3.7 jupyter matplotlib numpy scipy obspy pyasdf
```

In this way, other packages can be added to your workflow (e.g., pandas, cartopy, basemap), without disrupting other projects or other environments. To activate the environment and start a new Jupyter Notebook Session in your webbrowser:

```
$ conda activate beamforming_environment
$ jupyter notebook
```

Last updated: August 2020. For questions or suggestions: daniel.bowden@erdw.ethz.ch
