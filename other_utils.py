import matplotlib.pyplot as plt
import numpy as np
import time
import obspy as ob


def spec_shift(signal,shift):
	"""
	shifted_signal = specshift(signal,shift)

	shift a time-domain waveform by some number of samples, 
	 This can handle arbitrary sub-sample shifts. (good!)
	 This is rather computationally inefficient. (bad)
	
	Alternatively, one might apply shifts just by indexing, i.e.:
	 data_shifted[0:50,ii] = data[43:93,ii]
	
	Modified from matlab scripts from Jean-Paul Ampuero, ampuero@erdw.ethz.ch

	INPUT:
	------
	signal:			data [nsamples, nstations]
	shift:			shifts [nstations], must be number of samples, not seconds        
	-----

	To get a numpy array of signals, instead of the obspy stream object:
		nsta = np.shape(stations)[1]
		nsamp = np.size(stZ[0].data)
		data = np.zeros([nsamp,nsta])
		for ii in range(nsta):
    			data[:,ii] = stZ[ii].data


	"""
	# Assumes signal is size (nsamp, nstations)
	# Assumes shift is size (nstations)
	nshift = np.size(shift)
	#[nsamp,nsta] = np.shape(np.atleast_2d(signal))
	if( len(signal.shape) > 1):
	    nsamp = signal.shape[0]
	    nsta  = signal.shape[1]
	else:
	    nsamp = signal.shape[0]
	    nsta = 1
	    signal=signal[:,np.newaxis]


	if(nshift!=nsta):
	    print("error! incorrect sizes")
	    return -1

	# NFFT, as next power of two, with padding for wraparound
	n=int(2.0**np.ceil(np.log2(nsamp+np.max(nshift))))
	n2=int(n/2)
	spect=np.fft.fft(signal,n,axis=0)
	ik = np.zeros([n,1],dtype='complex')
	ik[:n2+1,0] = 2*1j*np.pi*np.arange(0,n2+1)/n
	ik[n2+1:,0]= -ik[ np.arange(n2-1,0,-1),0]
	fshift=np.exp(ik*shift)
	fshift[n2+1] = np.real(fshift[n2+1]) #real nyquist freq.
	spect = spect*fshift
	signalout = np.fft.irfft(spect,n,axis=0)[0:nsamp,:]
	return signalout

def get_spectra(waveforms, plot=True):
	"""
	ff,spectra = get_spectra(waveforms)

	OBSPY has some nice spectral functions, but we'll define things manually here
	"""

	# nfft will be a power of 2.
	# the number of points returned by rfft is only half (+1) of this
	nsta = np.shape(waveforms)[0]
	npts = waveforms[0].stats.npts
	nfft = (2.0**np.round(np.log2(npts))).astype(int)
	nsave = np.int((nfft/2)+1)

	# Take fourier transforms of all our traces.
	delta = waveforms[0].stats.delta # timestep between regularly spaced samples.
	df=1.0/(2*nfft*delta)
	ff = np.linspace(0.0, 1.0/(2.0*delta), nsave)
	spectra = np.zeros([np.size(ff),nsta],dtype='complex')
	for ii,tr in enumerate(waveforms):
		spectra[:,ii] = np.fft.rfft(tr[:],n=nfft)
	
	if(plot):
		plt.loglog(1/ff, np.abs(spectra))
		plt.xlim([1,200])
		plt.ylim([10^-9,10^-3])
		plt.xlabel('Period [s]')
		plt.ylabel('FFT output [units?]')
		plt.show()

	return ff, spectra

def cross_correlate(waveforms, stations, max_lag=200, verbose=True, plot=True):
	"""
	ff,ccf,tt,cct,xx,yy,pair_names = cross_correlate(waveforms, stations, verbose=True, plot=True)

	"""

	#- Set up a few things: how many station pairs, how long should the correlations be, etc.
	nsta = np.shape(waveforms)[0]
	npts = waveforms[0].stats.npts
	dt = waveforms[0].stats.delta

	# Get the station locations / distances for plotting 
	lats = [ sta.latitude for sta in stations[0].stations]
	lons = [ sta.longitude for sta in stations[0].stations]
	elevs = [ sta.elevation for sta in stations[0].stations]
	names = [ sta.code for sta in stations[0].stations]

	ref_lat = np.mean(lats)
	ref_lon = np.mean(lons)
	xx=np.zeros(nsta,)
	yy=np.zeros(nsta,)
	for i in range(nsta):
	    xx[i],yy[i] = ob.signal.util.util_geo_km(ref_lon, ref_lat, lons[i], lats[i])

	npairs = 0
	pair_names = []
	for i in range(nsta):
		for j in range(nsta):
			pair_names.append(names[i] + '-' + names[j])
			npairs+=1

	if(verbose):
		print('Will compute {0} pairs, keeping +/- {1} seconds of correlation'.format(npairs,max_lag))
	        
	imax = np.int(max_lag/dt)
	tt = np.arange(-max_lag,max_lag,dt)
	
	#- nfft will be a power of 2.
	#- the number of points returned by rfft is only half (+1) of this
	nfft = (2.0**np.round(np.log2(npts))).astype(int)
	nsave = np.int((nfft/2)+1)
	ff,spectra = get_spectra(waveforms, plot=False)

	#- Empty arrays for cross-correlations in frequency and time domain
	ccf=np.zeros([nsave,npairs],dtype=complex)
	cct=np.zeros([len(tt),npairs])
	if(verbose):
		print('freq-domain correlations -> "ccf" size: {0}'.format(np.shape(ccf)))
		print('time-domain correlations -> "cct" size: {0}'.format(np.shape(cct)))

	ipair = 0
	for i in range(nsta):
		for j in range(nsta):
			ccf[:,ipair] = spectra[:,i]*np.conj(spectra[:,j])
			ipair+=1

	#- IFFT to go back to time domain
	dummy=np.real(np.fft.ifft(ccf,axis=0,n=nfft)/dt)
	#- re-arrange (equivalent to np.ifftshift())
	cct[imax:imax*2,:]=dummy[0:imax,:]
	cct[0:imax,:]=dummy[nfft-imax:nfft,:]


#	if(plot):
#		#- See how the re-arranging of "dummy" into "cct" worked out
#		plt.plot(dummy[:,40])
#		plt.plot([imax,imax],[-2e7,2e7],'k')
#		plt.plot([nfft-imax,nfft-imax],[-2e7,2e7],'k')
#		plt.plot([nfft,nfft],[-2e7,2e7],'k')
#		plt.show()
#		
#		plt.plot(cct[:,40])
#		plt.show()

	# Plot record section of the noise correlations
	if(plot):
		fig, ax = plt.subplots()
		counter=0
		for i in range(nsta):
			for j in range(nsta):
				if(i<j): # Only plot one direction, don't need both for this plot
					total_distance = np.sqrt( (xx[i]-xx[j])**2 + (yy[i]-yy[j])**2 )
					this_corr = cct[:,counter]
					this_corr = this_corr / np.max(this_corr) * 10
					plt.plot(tt, this_corr + total_distance,'k', linewidth=.5)
				counter += 1
		        
		        
		# The FASTEST we expect strong arrivals is that of surface waves, ~3km/s
		# If inhomogeneous noise sources are used, the actual signals will appear to arrive faster
		# (i.e., inside of the "v")
		ylim = ax.get_ylim()
		vel = 3
		print(ylim)
		print(ylim[1]/vel)
		plt.plot([0,ylim[1]/vel],[0,ylim[1]],'r')
		plt.plot([0,-ylim[1]/vel],[0,ylim[1]],'r')
		
		plt.xlabel('Time [sec]')
		plt.ylabel('Station-station Distance [km]')
		plt.show()
	return ff, ccf, tt, cct, xx, yy, pair_names


