# Other
import os
import obspy as ob
import obspy.signal

from obspy.clients.fdsn import Client
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import pyasdf
from pyasdf import ASDFDataSet
from obspy.core.inventory import Inventory, Network, Station, Channel, Site

from inventory_utils import *



def downsample_and_filter(trace):
	#- The waveforms that come out of the simulation have a very high sample rate, because of the timestepping
	#-  needed for spectral element simulations. We can downsample this.
	#-  Given that our highest useable frequency is 1/20seconds = 0.05Hz
	#-  our Nyquist (x2) is 0.1Hz. Let's just keep it at 1Hz to be safe
	
	#- Additionally, this particular simulation was only designed for waveforms from 20 seconds to 100 seconds periods
	#-  so we'll filter out everything else
	trace.detrend('linear')
	trace.taper(max_percentage=0.05, type='cosine')
	trace.filter("bandpass", freqmin=1/100.0, freqmax=1/20.0, corners=2)
	trace.resample(1)
	return trace

def load_salvus_waveforms(waveform_dir, verbose=True, plot=True):
	stations = obspy.clients.fdsn.Client("IRIS").get_stations(
	    network="XD", station="HOT*", channel="BHZ,BHN,BHE",location="--",level="channel", format="text", 
	    starttime="1996-08-26T00:00:00",endtime="1996-09-26T00:00:00"
	)
	#- We want to write to an XML file, but have to fix a couple values for consistency with SALVUS output
	#- This will update metadata like sample rate, channel names, and give a flat instrument response.
	stations = setup_simulation_inventory(stations)
	stations.write("station.xml", format="stationxml", validate=True)
	
	#- Next time, you could alternatively just read in this xml file
	#stations = ob.read_inventory("station.xml")
	
	output = ASDFDataSet(os.path.join('output_onesource_ricker','receivers.h5'), mode="r")


	
	#- We'll make an empty obspy stream for each of the 3 components separately
	waveformsZ = obspy.Stream()
	waveformsN = obspy.Stream()
	waveformsE = obspy.Stream()

	for i, waveform in enumerate(output.waveforms):
		trace = waveform.velocity.select(component="Z")[0]
		trace = downsample_and_filter(trace)
		trace.stats.channel = "BHZ"
		waveformsZ.append(trace)
	    
		trace = waveform.velocity.select(component="N")[0]
		trace = downsample_and_filter(trace)
		trace.stats.channel = "BHN"
		waveformsN.append(trace)
		
		trace = waveform.velocity.select(component="E")[0]
		trace = downsample_and_filter(trace)
		trace.stats.channel = "BHE"
		waveformsE.append(trace)

	for waveformsX in [waveformsZ,waveformsN,waveformsE]:
		for trace in waveformsX:
			this_sta = stations.select(station=trace.stats.station)[0][0][0]
			trace.stats["coordinates"] = {} 
			trace.stats["coordinates"]["latitude"] = this_sta.__dict__['_latitude']
			trace.stats["coordinates"]["longitude"] = this_sta.__dict__['_longitude']
		    
	if(verbose):
		print('---------Stream------------')
		print('WaveformsZ:')
		print(waveformsZ)

		print('---------Trace------------')
		print('Example: waveformsZ[0]:')
		print(waveformsZ[0].stats)

	#-------------------------------------------------------------------------------
	#- Remove instrument response. Only needed for real data

	#- First, make a copy so we don't overwrite or mess up the raw data
	#- OBSPY (and many things in Python) should use a "deepcopy" function
	# waveformsZ2 = waveformsZ.copy()
	
	#-  Seismometers record arbitrary voltages, so we need to convert that to actual physical units
	#-   for displacement / velocity / acceleration. 
	#-  First, define a range of frequencies to keep (0.005 to 10), with gently tapered sides

	# pre_filt = [0.001, 0.005, 10, 20]
	# waveformsZ2.remove_response(inventory=stations, pre_filt=pre_filt, output="VEL",
	#                    water_level=60,plot=False)                           # Try plot=True to see more.
	
	#-------------------------------------------------------------------------------
	
	if(plot):
		print('--------Example Plot------')

		# Get absolute times (in seconds)
		tt = waveformsZ[29].times
		
		# Actually we'll plot against times formatted like HH:MM:SS
		fig = plt.figure(figsize=[20,5])
		ax = fig.add_subplot(1, 1, 1)
		ax.plot(waveformsZ[29].times("matplotlib"), waveformsZ[0].data, "k-", linewidth=1)
		ax.plot(waveformsN[29].times("matplotlib"), waveformsN[0].data, "r-", linewidth=1)
		ax.plot(waveformsE[29].times("matplotlib"), waveformsE[0].data, "b-", linewidth=1)
		ax.legend(['Z','N','E'])
		ax.xaxis_date()
		fig.autofmt_xdate()
		plt.show()


		print('---------waveformsZ.plot(type="section")------------')
		waveformsZ.plot(type='section', ev_coord = (71.90247861471832, 13.547921720280172), dist_degree=True)


		print('---------Station map--------------------')
		lats = [ sta.latitude for sta in stations[0].stations]
		lons = [ sta.longitude for sta in stations[0].stations]
		elevs = [ sta.elevation for sta in stations[0].stations]
		names = [ sta.code for sta in stations[0].stations]
		#- Rather than work with lat/lon, we can convert to kilometers in x/y
		#- This will be helpful for calculating distances later
		ref_lat = np.mean(lats)
		ref_lon = np.mean(lons)
		
		xx=np.zeros(np.size(stations[0]),)
		yy=np.zeros(np.size(stations[0]),)
		
		#- This command would compute the distance between any two lat/lon points
		#distance = ob.geodetics.calc_vincenty_inverse(ref_lat, ref_lon, lats[0], lons[0])
		
		#- But there's another OBSPY tool that automatically breaks down this distance further into X, Y components
		print(np.shape(stations[0]))
		for i in range(np.shape(stations[0])[0]):
		    xx[i],yy[i] = ob.signal.util.util_geo_km(ref_lon, ref_lat, lons[i], lats[i])
		    
		plt.plot(xx,yy,'rv')
		for ii in range(np.size(names)):
		    plt.text(xx[ii],yy[ii],names[ii])
		plt.xlabel('Distance [km]')
		plt.ylabel('Distance [km]')
		plt.axis('equal')
		plt.show()

	return stations, waveformsZ, waveformsN, waveformsE









