# Copied from Adam Ringler's unmerged branch, 11.06.2020
# https://github.com/aringler-usgs/obspy/tree/flat_response
from obspy.core.inventory.response import PolesZerosResponseStage,InstrumentSensitivity,Response
from obspy.core.inventory import Inventory, Network, Station, Channel, Site
import obspy


if __name__ == "__main__":
    print('Error: Module run as script')   

def get_flat_response(sensitivity=1., frequency=1.,
                      input_units="M/S", output_units="M/S",
                      include_pole_zero_at_zero=False):

    """
    Makes a flat response object.
    :type sensitivity: float, optional
    :param sensitivity : The sensitivity of the response, defaults to 1.
    :type frequency: float, optional
    :param frequency: The frequency where the sensitivity is stated,
        defaults to 1 Hz.
    :type input_units: string, optional
    :param input_units: The input units going into the response object,
        default units are "M/S"
    :type output_units: string, optional
    :param input_units: The output units going out of the response object,
        default units are "M/S"
    :type include_pole_zero_stage: bool, optional
    :param include_pole_zero_at_zero: This option allows for a pole/zero
        stage to be include. If a pole zero stage is included it adds
        one pole and one zero both with the value of 0 + 0j.  This allows
        for the user to apply a deconvolution routine.  E.g. it allows
        the user to include the tapering, detrendings, and filtering
        during the response removal.
    """
    instrument_sensitivity = InstrumentSensitivity(sensitivity,
                                                   frequency,
                                                   input_units,
                                                   output_units)
    stage = []
    pzt = "LAPLACE (HERTZ)"
    zeros = []
    poles = []
    if include_pole_zero_at_zero:
        zeros.append(0. + 0.j)
        poles.append(0. + 0.j)
    stg1 = PolesZerosResponseStage(stage_sequence_number=1,
                                   stage_gain=sensitivity,
                                   stage_gain_frequency=frequency,
                                   input_units=input_units,
                                   output_units=output_units,
                                   pz_transfer_function_type=pzt,
                                   normalization_frequency=frequency,
                                   zeros=zeros, poles=poles)
    stage.append(stg1)
    response = Response(instrument_sensitivity=instrument_sensitivity,
                   response_stages=stage)
    return response


def setup_simulation_inventory(inv=None):
    resp = get_flat_response()
    inv2 = Inventory(
        # We'll add networks later.
        networks=[],
        source="Simulated")

    net = Network(
        code="XD",
        start_date=obspy.UTCDateTime(1969, 1, 1),
        stations=[]
        )

    for real_station in inv[0].stations:
        sta = Station(
            code     =real_station.code,
            latitude =real_station.latitude,
            longitude=real_station.longitude,
            elevation=real_station.elevation,
            creation_date=obspy.UTCDateTime(1969, 1, 1),
            site=Site(name="based on a real station")
            )

        for cmp in ["BHZ","BHN","BHE"]:
            real_channel=inv.select(channel=cmp,station=real_station.code)[0][0][0]
            cha = Channel(
                code=real_channel.code,
                location_code="",
                latitude =real_channel.latitude,
                longitude=real_channel.longitude,
                elevation=real_channel.elevation,
                depth=real_channel.depth,
                azimuth=real_channel.azimuth,
                dip=real_channel.dip,
                sample_rate=10,
                response=resp,
                start_date=obspy.UTCDateTime(1969, 1, 1)
                )
            sta.channels.append(cha)
        net.stations.append(sta)
    inv2.networks.append(net)

    return inv2
